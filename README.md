# bulki

One-html-file image resizer, cropper and renamer. Initially made for dreambooth image preparation. Can make square or non-square images of defined size and you can also position them ^^

Here is online [demo](http://apps.kajutastudio.cz/bulki.html).


## credits

https://github.com/photopea/UZIP.js

https://pixabay.com/photos/bread-homemade-bread-home-made-786145/

resolution list (for StableTuner bucketing) provided by : Shuteye_491
